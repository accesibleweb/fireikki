const validar = document.querySelector("#validar");
const dejados = document.querySelector(".dejados");
let sectionAnt = document.querySelector(".actividad");
let sectionActual = sectionAnt.cloneNode(true);
if (validar) {
  validar.setAttribute("onclick", "validos()");
}
function validos() {
  let deja = dejados.querySelectorAll("button");
  let count = 0;
  let empty = 0;
  for (let index = 0; index < deja.length; index++) {
    if (arr[index] === deja[index].innerHTML) {
      count++;
    }
    if (deja[index].innerHTML.length > 0) {
      empty++;
    }
  }
  if (empty === deja.length) {
    validar.innerHTML =
      "<img src='../../imagenes/restart.png' alt='ícono: flecha en forma circular, indica realizar otravéz'> intentar de nuevo";
    validar.setAttribute("onclick", "codeAnte()");
    validar.classList.remove("btn-bac-grnd");
    validar.classList.add("btn-bac-od");
    /*     count = 0;
    empty = 0; */
  }
  check(count, deja.length);
}
function check(total, cuantos) {
  if (total === cuantos) {
    texto = `
    <h2>¡Felicitaciones!</h2>
    <p>Acertaste en la actividad</p>
    <button>Aceptar</button>
    `;
  } else {
    texto = `
    <h2>¡Vuelve a intentarlo!</h2>
    <p>Tú puedes  encontrar la respuesta correcta</p>
    <button onclick="closePop()">Aceptar</button>
    `;
  }
  pop(texto);
}
function closePop() {
  document.querySelector(".oscuro").remove();
  validar.focus();
}
function pop(infor) {
  const validar = document.querySelector("#validar");
  let newItem = document.createElement("div");
  newItem.setAttribute("class", "oscuro");
  let newItemWhite = document.createElement("div");
  newItemWhite.setAttribute("class", "iluminado");
  newItemWhite.setAttribute("tabindex", 0);
  newItemWhite.innerHTML = infor;
  newItem.appendChild(newItemWhite);
  let list = validar.parentElement;
  console.log(list);
  list.appendChild(newItem);
  newItemWhite.focus();
  const listado = document.querySelector(".listado");
  const notify = document.querySelector(".notify");
  if (listado) {
    listaOut();
  }
  if (notify) {
    cloudsOut();
  }
}
function codeAnte() {
  sectionAnt.innerHTML = sectionActual.innerHTML;
}
