let btnMenu = document.querySelector("#menu");
let cerrarMenu = document.querySelector("#xMenu");
var modalMenu = document.getElementById("pop");
let estateMenu = 0;
cerrarMenu.setAttribute("onclick", "closeMenu()");
cerrarMenu.setAttribute("onkeydown", "closeMenuBtn(event)");
btnMenu.setAttribute("onclick", "openMenu()");

function openMenu() {
  modalMenu.setAttribute(`onkeydown`, `runMenuCount(this, event)`);
  modalMenu.style.display = "block";
  modalMenu.setAttribute("tabindex", 0);
  modalMenu.focus();
  cloudsOut();
  if (listado) {
    closeMenu();
  }
  const lista = document.querySelectorAll("#pop li");
  for (let index = 0; index < lista.length; index++) {
    lista[index].setAttribute("onkeydown", `runMenuList(this, event)`);
  }
}
function runMenuCount(este, e) {
  este.removeAttribute("onkeydown");
  este.removeAttribute("tabindex");
  const lista = document.querySelectorAll("#pop a");
  runMenuList(lista[0], e, true);
}
function runMenuList(este, e, first = "") {
  let lista = document.querySelectorAll("#pop li a");
  let listaNum = lista.length - 1;
  let key = e.which || e.keyCode;
  if (first) {
    runMenuItemList(lista);
    return false;
  }

  switch (key) {
    case 38:
      estateMenu--;
      if (estateMenu < 0) {
        closeMenu();
        document.querySelector("#menu").focus();
        estateMenu = 0;
        return;
      }
      runMenuItemList(lista);
      break;
    case 40:
      estateMenu++;
      if (estateMenu > listaNum) {
        document.querySelector("#pop #xMenu").focus();
        estateMenu = lista;
      } else {
        runMenuItemList(lista);
      }
      break;
    case 27:
      closeMenu();
      let dataBtn2 = este.parentElement.previousElementSibling;
      dataBtn2.focus();
      break;
  }
}
function runMenuItemList(lista) {
  for (let index = 0; index < lista.length; index++) {
    lista[index].removeAttribute("tabindex", "0");
  }
  lista[estateMenu].setAttribute("tabindex", "0");
  lista[estateMenu].focus();
}

function closeMenuBtn(e) {
  let key = e.which || e.keyCode;
  if (key === 40) {
    closeMenu();
  }
  if (key === 38) {
    let lista = document.querySelectorAll("#pop li a");
    console.log(lista.length);
    lista[lista.length - 1].focus();
    estateMenu = lista.length - 1;
  }
}

function closeMenu() {
  estateMenu = 0;
  modalMenu.style.display = "none";
  btnMenu.focus();
  cuerpo.removeAttribute("onclick");
}
