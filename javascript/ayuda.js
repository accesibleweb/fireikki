var cloud = document.getElementsByClassName("cloud")[0];
if (cloud) {
  cloud.addEventListener("click", clouds);
}

function clouds() {
  var not = document.getElementsByClassName("notify")[0];
  not.setAttribute("tabindex", "0");
  not.innerHTML = cloudInfo("true");
  not.style.display = "block";
  not.focus();
  let listado = document.querySelector(".listado");
  if (listado) {
    listaOut();
  }
}

function cloudInfo() {
  var info;
  var slid = document.querySelectorAll(".slider");
  let video = document.querySelectorAll("iframe");
  let titulo = document.querySelectorAll("h2");
  let drag = document.querySelectorAll(".arrastrados");
  switch (true) {
    case slid.length > 0:
      info = "Ingresa a los botones enumerados para leer cada diapositiva ";
      break;
    case video.length > 0:
      info =
        "Ingresa en el botón de reproducir para activar el video, ingresa al documento descriptivo para tener el texto del video";
      break;
    case titulo[0].innerHTML === "Contenido":
      info = "Lee el texto e ingresa a cada enlace con cada capítulo";
      break;
    case titulo[0].innerHTML === "Recursos Complementarios":
      info =
        "Ingresa a cada enlace y descarga documentos que ayudan a tú aprendizaje";
      break;
    case drag.length > 0:
      info =
        "Arratre las opciones y dejalas en los espacios en blanco o abra la lista de los espacios en blanco y seleccione la opción correspondiente";
      break;
    default:
      info = "Lee la información que se presenta a continuación";
      break;
  }
  return info;
}

function cloudsOut() {
  var notify = document.getElementsByClassName("notify")[0];
  notify.style.display = "none";
  cuerpo.removeAttribute("onclick");
}
