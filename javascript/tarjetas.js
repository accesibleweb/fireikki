let tarjetas = document.querySelectorAll(".tarjeta");
if (tarjetas) {
  window.onload = () => {
    let alto = screen.height;
    let altura = (alto / 100) * 36;
    for (let index = 0; index < tarjetas.length; index++) {
      if (tarjetas[index].getBoundingClientRect().top > altura) {
        tarjetas[index].classList.add("opa-0");
      } else {
        tarjetas[index].classList.add("escala1");
      }
    }
  };
  window.onscroll = () => {
    for (let index = 0; index < tarjetas.length; index++) {
      let alto = screen.height;
      let altura = (alto / 100) * 50;
      if (!tarjetas[index].classList.contains("escala1")) {
        if (tarjetas[index].getBoundingClientRect().top < altura) {
          tarjetas[index].classList.add("escala1");
          tarjetas[index].classList.remove("opa-0");
          break;
        }
      }
    }
  };
}
function addClassCard(indice, altura) {
  for (let index = 0; index < tarjetas.length; index++) {
    if (tarjetas[index].getBoundingClientRect().top > altura) {
      tarjetas[index].classList.add("escala1");
      tarjetas[index].classList.remove("opa-0");
      break;
    }
  }
}
