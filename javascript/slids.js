const sliderClass = document.querySelectorAll(".slider");
var slideIndex = 1;
/* slider(slideIndex); */

function currentSlide(n) {
  slider((slideIndex = n));
}

function slider(n) {
  var i;
  var x = document.querySelectorAll(".slider div");
  var dots = document.getElementsByClassName("dot");
  let slid = sliderClass[0].querySelectorAll("div");

  if (n > x.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = x.length;
  }
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
    slid[i].removeAttribute("tabindex");
  }

  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" bac-bc-y", "");
  }

  x[slideIndex - 1].style.display = "block";
  dots[slideIndex - 1].className += " bac-bc-y";
  slid[slideIndex - 1].setAttribute("tabindex", "0");
  slid[slideIndex - 1].focus();
}
