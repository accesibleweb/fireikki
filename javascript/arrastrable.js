var arrastra = document.querySelectorAll(".arrastra");

var deja = document.querySelectorAll(".deja");
const menu = document.querySelector("#pop");
let estate = 0;
document.addEventListener("DOMContentLoaded", function () {
  for (i = 0; i < arrastra.length; i++) {
    arrastra[i].setAttribute("draggable", "true");
    arrastra[i].setAttribute("ondragstart", "drag(event)");
  }
  for (i = 0; i < deja.length; i++) {
    deja[i].setAttribute("ondrop", "drop(event)");
    deja[i].setAttribute("ondragover", "allowDrop(event)");
    deja[i].setAttribute("data", i);
  }
});

if (deja) {
  for (let index = 0; index < deja.length; index++) {
    deja[index].setAttribute("onclick", "dejar(this)");
  }
}

function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.innerHTML);
}

function drop(ev) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData("text");
  returnBtn(ev.target.innerHTML);
  ev.target.innerHTML = data;
  intoText(data);
}

function dejar(este) {
  const listadoExist = document.querySelector(".listado");
  const footer = document.querySelector("footer");

  if (menu.style.display === "block") {
    menu.style.display = "none";
  }
  if (notify) {
    cloudsOut();
  }
  if (listadoExist) {
    listaOut();
  }
  let donde = este.parentElement;
  let lista = document.createElement("ul");
  let listado = document.querySelectorAll(".arrastra");
  let alturaLista = 27 * listado.length;
  lista.setAttribute("onkeydown", "runCount(this,event)");
  lista.setAttribute("role", "listbox");
  lista.setAttribute("tabindex", "-1");

  if (
    este.getBoundingClientRect().top + alturaLista >
    footer.getBoundingClientRect().top
  ) {
    let margenUp =
      este.getBoundingClientRect().top +
      alturaLista -
      footer.getBoundingClientRect().top;
    lista.style.marginTop = -Math.floor(margenUp) + "px";
  }
  for (let index = 0; index < listado.length; index++) {
    let listaItem = document.createElement("li");
    if (listado[index].getAttribute("style") !== "display: none;") {
      listaItem.setAttribute("onkeydown", `runList(this, event)`);
      listaItem.innerHTML = listado[index].innerHTML;
      listaItem.setAttribute("onclick", "pasoLista(this)");
      listaItem.setAttribute("role", "option");
      lista.appendChild(listaItem);
    }
  }
  lista.setAttribute("class", "listado");
  donde.appendChild(lista);
  const listadoFocus = document.querySelector(".listado");
  listadoFocus.setAttribute("tabindex", 0);
  listadoFocus.focus();
}

function intoText(text) {
  for (let index = 0; index < arrastra.length; index++) {
    let actual = arrastra[index];
    if (actual.innerHTML == text && actual.style.display != "none") {
      actual.style.display = "none";
      return;
    }
  }
}

function runCount(este, e) {
  este.removeAttribute("onkeydown");
  este.removeAttribute("tabindex");
  const lista = document.querySelectorAll(".listado li");
  runList(lista[0], e, true);
}
function runList(este, e, first = "") {
  let lista = document.querySelectorAll(".listado li");
  let listaNum = lista.length - 1;
  let key = e.which || e.keyCode;
  if (first) {
    runItemList(lista);
    return false;
  }

  switch (key) {
    case 38:
      if (estate <= 0) {
        estate = 0;
        return;
      }
      estate--;
      runItemList(lista);
      break;
    case 40:
      estate++;
      if (estate > listaNum) {
        estate = 0;
        listaOut();
        let dejados = document.querySelector(".dejados");
        dejados.setAttribute("tabindex", "0");
        dejados.focus();
        return;
      }
      runItemList(lista);
      break;
    case 13:
      pasoLista(este);
      break;
    case 27:
      listaOut();
      let dataBtn2 = este.parentElement.previousElementSibling;
      dataBtn2.setAttribute("tabindex", "0");
      dataBtn2.focus();
      break;
  }
}
function runItemList(lista) {
  for (let index = 0; index < lista.length; index++) {
    lista[index].classList.remove("bac-ycl");
    lista[index].removeAttribute("tabindex", "0");
    lista[index].removeAttribute("aria-selected", "true");
  }
  lista[estate].classList.add("bac-ycl");
  lista[estate].setAttribute("aria-selected", "true");
  lista[estate].setAttribute("tabindex", "0");
  lista[estate].focus();
}
function pasoLista(li) {
  let lista = li;
  let pasoText = li.innerHTML;
  let dataBtn = lista.parentElement.previousElementSibling;
  let dejados = document.querySelector(".dejados");
  returnBtn(dataBtn.innerHTML);
  dataBtn.innerHTML = pasoText;
  dataBtn.setAttribute("aria-label", "palabra seleccionada: " + pasoText);
  intoText(pasoText);
  listaOut();
  dejados.setAttribute("tabindex", "0");
  dejados.focus();
}
function listaOut() {
  const listadoExist = document.querySelector(".listado");
  listadoExist.remove();
  estate = 0;
}

function returnBtn(text) {
  for (let index = 0; index < arrastra.length; index++) {
    let actual = arrastra[index];
    if (actual.innerHTML == text && actual.style.display == "none") {
      actual.style.display = "block";
      return;
    }
  }
}
