var conten = document.getElementById("conte");

function parrafo() {
  var section = conten.getElementsByTagName("section");
  var text = section[0].innerHTML;
  section[0].innerHTML = text + "<p contenteditable>Nuevo parrafo</p>";
}
function img() {
  var section = conten.getElementsByTagName("section");
  var text = section[0].innerHTML;
  section[0].innerHTML = text + "<p contenteditable>Nuevo parrafo</p>";
}
function nodos() {
  var span = conten.querySelectorAll("span");
  for (var i = 0; i < span.length; i++) {
    span[i].removeAttribute("contenteditable");
  }
}
function calibrar() {
  var avance = conten.getElementsByClassName("avance")[0];
  var span = avance.getElementsByTagName("span");
  var num = parseInt(span[0].innerHTML);
  var links = avance.getElementsByTagName("a");
  switch (true) {
    case num <= 1:
      avance.removeChild(avance.childNodes[3]);
      links[1].setAttribute("href", num + 1 + ".html");
      break;
    case num > 2 && num < 28:
      links[0].setAttribute("href", num - 1 + ".html");
      links[1].setAttribute("href", num + 1 + ".html");
      break;
    case num >= 28:
      //avance.removeChild(avance.childNodes[9]);
      links[0].setAttribute("href", num - 1 + ".html");
      links[1].setAttribute("title", "Ir a la diapositiva de los créditos");
      links[1].setAttribute("href", "creditos.html");

      break;
  }
}
function crear() {
  nodos();
  calibrar();
  var contenidonuevo = conten.innerHTML;
  var contenedor = (document.getElementById("contenido").value =
    contenidonuevo);
}
